#include "stdafx.h"

#define DEVICE_ID	1

#define COM_PORT "COM5"
#define BAUD_RATE BaudRate::br9600

using namespace std;

typedef std::map<std::string, std::function<void(IAgirVisionBus* agb, const std::vector<std::string>& cmd)> > command_dict;

std::vector<std::string>& splitString(const std::string& s, char delim, std::vector<std::string>& elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}

void read(IAgirVisionBus* agb, const std::vector<std::string>& cmd) {
	DeviceMode masterMode;
	if (agb->getPLCMode(&masterMode) == Status::Ok) {
		cout << "PLC Mode: " << static_cast<int>(masterMode) << endl;
	}
	else {
		cout << "PLC Mode reading error " << endl;
	}

	CommandMode masterCommand;
	if (agb->getPLCCommand(&masterCommand) == Status::Ok) {
		cout << "PLC Command: " << static_cast<int>(masterCommand) << endl;
	}
	else {
		cout << "PLC Command reading error " << endl;
	}
}

void setPCMode(IAgirVisionBus* agb, const std::vector<std::string>& cmd) {
	if (cmd.size() < 2) {
		cout << "Unable to set PC mode: missing mode argument" << endl;
		return;
	}

	DeviceMode mode;
	if (cmd[1] == "start") {
		mode = DeviceMode::Start;
	}
	else if (cmd[1] == "stop") {
		mode = DeviceMode::Stop;
	}
	else {
		cout << "Invalid PC mode argument '" << cmd[1] << "'" << endl;
		return;
	}

	if (agb->setPCMode(mode) != Status::Ok) {
		cout << "Unable to set PC mode" << endl;
	}
}

void setPCCommandMode(IAgirVisionBus* agb, const std::vector<std::string>& cmd) {
	if (cmd.size() < 2) {
		cout << "Unable to set PC command mode: missing mode argument" << endl;
		return;
	}

	CommandMode mode;
	if (cmd[1] == "ready") {
		mode = CommandMode::CoordinateReady;
	}
	else if (cmd[1] == "disabled") {
		mode = CommandMode::CoordinateDisabled;
	}
	else {
		cout << "Invalid PC command mode argument '" << cmd[1] << "'" << endl;
		return;
	}

	if (agb->setPCCommandMode(mode) != Status::Ok) {
		cout << "Unable to set PC command mode" << endl;
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	command_dict commands;

	commands.insert(std::pair<std::string, std::function<void(IAgirVisionBus* agb, const std::vector<std::string>& cmd)> >("read", &read));
	commands.insert(std::pair<std::string, std::function<void(IAgirVisionBus* agb, const std::vector<std::string>& cmd)> >("mode", &setPCMode));
	commands.insert(std::pair<std::string, std::function<void(IAgirVisionBus* agb, const std::vector<std::string>& cmd)> >("command", &setPCCommandMode));

	IAgirVisionBus* agb = createAgirVisionBusMaster();

	Status status = agb->connect(COM_PORT, BAUD_RATE, Parity::None, DataBits::db8, StopBits::sb1, DEVICE_ID);

	if (status != Status::Ok) {
		cout << "Unable to connect to port '" << COM_PORT << "', error = " << static_cast<int>(status) << endl;
		cout << "MODBUS test program finished" << endl;
		return 0;
	}

	cout << "MODBUS test program started on port '" << COM_PORT << "' using baud rate " << static_cast<int>(BAUD_RATE) << endl;

	cout << "Use ctrl-d to quit the program" << endl;

	std::string input;
	while (std::getline(std::cin, input)) { // quit the program with ctrl-d
		if (input.size() == 0) {
			read(agb, std::vector<std::string>());
			continue;
		}

		std::vector<std::string> command_elements;
		splitString(input, ' ', command_elements);

		auto it = commands.find(command_elements[0]);

		if (it != end(commands)) {
			(it->second)(agb, command_elements); // execute the command
		}
		else {
			cout << "Command \"" << input << "\" not known" << endl;
		}
	}

	/*int coord = -5;

	for (int i = 0; i < 5; i++) {
		agb->setCoordinate(coord++);

		agb->setSlaveMode(DeviceMode::Start);
		agb->setSlaveCommandMode(CommandMode::CoordinateDisabled);

		std::this_thread::sleep_for(std::chrono::milliseconds(1500));

		agb->setCoordinate(coord++);

		agb->setSlaveMode(DeviceMode::Stop);
		agb->setSlaveCommandMode(CommandMode::CoordinateReady);

		std::this_thread::sleep_for(std::chrono::milliseconds(1500));
		
		DeviceMode masterMode;
		if (agb->getMasterMode(&masterMode) == Status::Ok) {
			cout << "masterMode: " << static_cast<int>(masterMode) << endl;
		}

		CommandMode masterCommand;
		if (agb->getMasterCommand(&masterCommand) == Status::Ok) {
			cout << " masterCommand: " << static_cast<int>(masterCommand) << endl;
		}
	}
	
	std::this_thread::sleep_for(std::chrono::milliseconds(7000));*/
			
	status = agb->close();

	cout << "MODBUS test program finished" << endl;

	return 0;
}


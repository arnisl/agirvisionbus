#pragma once

#include "AgirVisionBusBase.h"

class AgirVisionBusMaster : public AgirVisionBusBase {
public:
	AgirVisionBusMaster() : AgirVisionBusBase() {}
	~AgirVisionBusMaster() { close(); }
	
	Status connect(const char* device, BaudRate baudrate, Parity parity, DataBits dataBits, StopBits stopBits, int deviceId);
	Status close();
	
protected:
	Status getRegister(int registerAddress, uint16_t* value);
	Status setRegister(int registerAddress, uint16_t value);
};
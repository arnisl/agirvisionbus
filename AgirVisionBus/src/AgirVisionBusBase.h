#pragma once

#include "modbus/modbus.h"
#include "AgirVisionBus.h"

#define MASTER_MODE_ADDRESS		1023
#define	MASTER_COMMAND_ADDRESS	1024
#define SLAVE_MODE_ADDRESS		1025
#define	SLAVE_COMMAND_ADDRESS	1026
#define COORDINATE_ADDRESS		1027
#define ENCODER_ADDRESS			1028

#define FIRST_REGISTER_ADDRESS	MASTER_MODE_ADDRESS
#define LAST_REGISTER_ADDRESS	ENCODER_ADDRESS
#define REGISTER_COUNT			(LAST_REGISTER_ADDRESS - FIRST_REGISTER_ADDRESS + 1)

class AgirVisionBusBase : public IAgirVisionBus {
public:
	AgirVisionBusBase() : _modbus(nullptr) {}
	
	Status getPLCMode(DeviceMode* mode);
	Status getPLCCommand(CommandMode* mode);

	Status setPCMode(DeviceMode mode);
	Status setPCCommandMode(CommandMode mode);

	Status setCoordinate(short coordinate);

protected:
	virtual Status getRegister(int registerAddress, uint16_t* value) = 0;
	virtual Status setRegister(int registerAddress, uint16_t value) = 0;

	modbus_t* _modbus;
};
#include "AgirVisionBusBase.h"

Status AgirVisionBusBase::getPLCMode(DeviceMode* mode) {
	uint16_t value;
	Status status = getRegister(MASTER_MODE_ADDRESS, &value);

	if (status != Status::Ok) {
		return status;
	}
	if (value != STOP && value != START) {
		return Status::ValueOutOfRange;
	}

	*mode = static_cast<DeviceMode>(value);

	return status;
}

Status AgirVisionBusBase::getPLCCommand(CommandMode* mode) {
	uint16_t value;
	Status status = getRegister(MASTER_COMMAND_ADDRESS, &value);

	if (status != Status::Ok) {
		return status;
	}
	if (value != COORDINATE_DISABLED && value != COORDINATE_READY) {
		return Status::ValueOutOfRange;
	}

	*mode = static_cast<CommandMode>(value);

	return status;
}

Status AgirVisionBusBase::setPCMode(DeviceMode mode) {
	return setRegister(SLAVE_MODE_ADDRESS, static_cast<uint16_t>(mode));
}

Status AgirVisionBusBase::setPCCommandMode(CommandMode mode) {
	return setRegister(SLAVE_COMMAND_ADDRESS, static_cast<uint16_t>(mode));
}

Status AgirVisionBusBase::setCoordinate(short coordinate) {
	return setRegister(COORDINATE_ADDRESS, static_cast<uint16_t>(coordinate));
}
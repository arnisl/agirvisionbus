#include "errno.h"
#include "AgirVisionBusBase.h"
#include "AgirVisionBusSlave.h"

IAgirVisionBus* _cdecl createAgirVisionBusSlave() {
	return new AgirVisionBusSlave();
}

Status AgirVisionBusSlave::connect(const char* device, BaudRate baudrate, Parity parity, DataBits dataBits, StopBits stopBits, int deviceId) {
	if (_modbus != nullptr) {
		return Status::Error;
	}

	_modbus = modbus_new_rtu(device, static_cast<int>(baudrate), static_cast<char>(parity), static_cast<int>(dataBits), static_cast<int>(stopBits));
	if (_modbus == nullptr) {
		return errno == EINVAL ? Status::InvalidParams : Status::Error;
	}

	if (modbus_set_slave(_modbus, deviceId) == -1) {
		return errno == EINVAL ? Status::InvalidDeviceId : Status::Error;
	}

	_mapping = modbus_mapping_new(0, 0, MASTER_MODE_ADDRESS + REGISTER_COUNT, 0);
		
	if (modbus_connect(_modbus) == -1) {
		modbus_free(_modbus);
		_modbus = nullptr;

		return Status::Error;
	}

	_connected = true;
	_listenerThread = new std::thread(&AgirVisionBusSlave::listenerHandler, this);
			
	return Status::Ok;
}

void AgirVisionBusSlave::listenerHandler() {
	uint8_t* query = (uint8_t*)malloc(MODBUS_RTU_MAX_ADU_LENGTH);
		
	int header_length = modbus_get_header_length(_modbus);

	bool interruptedOnError = false;

	while (_connected) {
		int rc;
		do {
			rc = modbus_receive(_modbus, query);
		} while (rc == 0 && _connected);

		if (!_connected) {
			break;
		}
		if (rc == -1 && errno != EMBBADCRC) {			
			interruptedOnError = true;
			break;
		}

		_mutex.lock();
				
		rc = modbus_reply(_modbus, query, rc, _mapping);

		_mutex.unlock();
		
		if (rc == -1) {
			interruptedOnError = true;
			break;
		}
	}

	free(query);

	if (interruptedOnError) {
		_connected = false;
		destroy();
	}
}

void AgirVisionBusSlave::destroy() {
	free(_listenerThread);

	modbus_mapping_free(_mapping);

	modbus_close(_modbus);

	modbus_free(_modbus);
	_modbus = nullptr;
}

Status AgirVisionBusSlave::close() {
	if (_modbus == nullptr) {
		return Status::NotConnected;
	}

	_connected = false;
	_listenerThread->join();
	
	destroy();

	return Status::Ok;
}

Status AgirVisionBusSlave::getRegister(int registerAddress, uint16_t* value) {
	if (_modbus == nullptr) {
		return Status::NotConnected;
	}
	if (registerAddress < FIRST_REGISTER_ADDRESS || registerAddress > LAST_REGISTER_ADDRESS) {
		return Status::AddressOutOfRange;
	}

	_mutex.lock();

	*value = _mapping->tab_registers[registerAddress];

	_mutex.unlock();

	return Status::Ok;
}

Status AgirVisionBusSlave::setRegister(int registerAddress, uint16_t value) {
	if (_modbus == nullptr) {
		return Status::NotConnected;
	}
	if (registerAddress < FIRST_REGISTER_ADDRESS || registerAddress > LAST_REGISTER_ADDRESS) {
		return Status::AddressOutOfRange;
	}

	_mutex.lock();

	_mapping->tab_registers[registerAddress] = value;

	_mutex.unlock();

	return Status::Ok;
}



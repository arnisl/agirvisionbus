#pragma once

#include <thread>
#include <mutex>
#include "AgirVisionBusBase.h"

class AgirVisionBusSlave : public AgirVisionBusBase {
public:
	AgirVisionBusSlave() : AgirVisionBusBase(), _connected(false) {}
	~AgirVisionBusSlave() { close(); }
		
	Status connect(const char* device, BaudRate baudrate, Parity parity, DataBits dataBits, StopBits stopBits, int deviceId);
	Status close();

protected:
	Status getRegister(int registerAddress, uint16_t* value);
	Status setRegister(int registerAddress, uint16_t value);

private:
	void listenerHandler();
	
	void destroy();

	bool _connected;
	modbus_mapping_t* _mapping;

	std::thread* _listenerThread;
	std::mutex _mutex;
};
#include "errno.h"
#include "AgirVisionBusBase.h"
#include "AgirVisionBusMaster.h"

IAgirVisionBus* _cdecl createAgirVisionBusMaster() {
	return new AgirVisionBusMaster();
}

Status AgirVisionBusMaster::connect(const char* device, BaudRate baudrate, Parity parity, DataBits dataBits, StopBits stopBits, int deviceId) {
	if (_modbus != nullptr) {
		return Status::Error;
	}

	_modbus = modbus_new_rtu(device, static_cast<int>(baudrate), static_cast<char>(parity), static_cast<int>(dataBits), static_cast<int>(stopBits));
	if (_modbus == nullptr) {
		return errno == EINVAL ? Status::InvalidParams : Status::Error;
	}

	if (modbus_set_slave(_modbus, deviceId) == -1) {
		return errno == EINVAL ? Status::InvalidDeviceId : Status::Error;
	}

	if (modbus_connect(_modbus) == -1) {
		modbus_free(_modbus);
		_modbus = nullptr;

		return Status::Error;
	}

	return Status::Ok;
}

Status AgirVisionBusMaster::close() {
	if (_modbus == nullptr) {
		return Status::NotConnected;
	}

	modbus_close(_modbus);

	modbus_free(_modbus);
	_modbus = nullptr;

	return Status::Ok;
}

Status AgirVisionBusMaster::getRegister(int registerAddress, uint16_t* value) {
	if (_modbus == nullptr) {
		return Status::NotConnected;
	}
	if (registerAddress < FIRST_REGISTER_ADDRESS || registerAddress > LAST_REGISTER_ADDRESS) {
		return Status::AddressOutOfRange;
	}

	if (modbus_read_registers(_modbus, registerAddress, 1, value) == -1) {
		return Status::Error;
	}
	
	return Status::Ok;
}

Status AgirVisionBusMaster::setRegister(int registerAddress, uint16_t value) {
	if (_modbus == nullptr) {
		return Status::NotConnected;
	}
	if (registerAddress < FIRST_REGISTER_ADDRESS || registerAddress > LAST_REGISTER_ADDRESS) {
		return Status::AddressOutOfRange;
	}

	if (modbus_write_register(_modbus, registerAddress, value) == -1) {
		return Status::Error;
	}
	
	return Status::Ok;
}




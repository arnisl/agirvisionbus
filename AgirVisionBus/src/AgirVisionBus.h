#pragma once

#ifdef AGB_DLL_EXPORTS
#define DLLCALL __declspec(dllexport)                                          
#else
#define DLLCALL __declspec(dllimport)  
#endif

enum class BaudRate {
	br9600 = 9600,
	br19200 = 19200,
	br57600 = 57600,
	br115200 = 115200
};

enum class Parity : char {
	None = 'N',
	Even = 'E',
	Odd = 'O'
};

enum class DataBits {
	db5 = 5,
	db6 = 6, 
	db7 = 7,
	db8 = 8
};

enum class StopBits {
	sb1 = 1,
	sb2 = 2
};

#define STOP 0
#define START 1
#define COORDINATE_DISABLED 0
#define COORDINATE_READY	1

enum class DeviceMode {
	Stop = STOP,
	Start = START
};

enum class CommandMode {
	CoordinateDisabled = COORDINATE_DISABLED,
	CoordinateReady = COORDINATE_READY
};

enum class Status {
	Ok,
	NotConnected,
	InvalidParams,
	InvalidDeviceId,
	AddressOutOfRange,
	ValueOutOfRange,
	Error
};

class IAgirVisionBus {
public:
	virtual ~IAgirVisionBus() {}
	virtual Status connect(const char* device, BaudRate baudrate, Parity parity, DataBits dataBits, StopBits stopBits, int deviceId) = 0;
	virtual Status close() = 0;

	virtual Status getPLCMode(DeviceMode* mode) = 0;
	virtual Status getPLCCommand(CommandMode* mode) = 0;

	virtual Status setPCMode(DeviceMode mode) = 0;
	virtual Status setPCCommandMode(CommandMode mode) = 0;

	virtual Status setCoordinate(short coordinate) = 0;
};

extern "C" 
{
	DLLCALL IAgirVisionBus* _cdecl createAgirVisionBusSlave();
	DLLCALL IAgirVisionBus* _cdecl createAgirVisionBusMaster();
};